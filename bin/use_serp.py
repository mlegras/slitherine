#! /usr/bin/python3
# -*- coding: utf-8 -*-


import sys
import os

sys.path.insert(0, "/usr/local/lib/python3.7/site-packages")
import numpy as np
import pandas as pd
import serpentine as sp


# matrices d'entree
matA = np.loadtxt(sys.argv[1])
matB = np.loadtxt(sys.argv[2])

matA_theo = np.loadtxt(sys.argv[3])
matB_theo = np.loadtxt(sys.argv[4])

# jsp si utile
if len(sys.argv) == 7:
    thr_tot = int(sys.argv[5])
    thr_min = int(sys.argv[6])

# binning des 2 matrices
matA_b, matB_b, mat_ratio = sp.serpentin_binning(matA, matB, thr_tot, thr_min)
matA_theo_b, matB_theo_b, mat_theo_ratio = sp.serpentin_binning(
    matA_theo, matB_theo, thr_tot, thr_min
)

# output
np.savetxt("mat1_bin.csv", matA_b, delimiter=",")
np.savetxt("mat2_bin.csv", matB_b, delimiter=",")
np.savetxt("mat1_theo_bin.csv", matA_theo_b, delimiter=",")
np.savetxt("mat2_theo_bin.csv", matB_theo_b, delimiter=",")
