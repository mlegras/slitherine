.. Slitherine documentation master file, created by
   sphinx-quickstart on Thu Jun 30 13:29:40 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Slitherine's documentation!
======================================

.. meta::
   :description lang=en: Automate building, versioning, and hosting of your technical documentation continuously on Read the Docs.

c quoi Slitherine, ça sert à quoi?
Differencial statistics analysis
can use Slitherine  + HiCcompare
+ choose to bin matrices and have new analysis then 


Getting Started
---------------

* :doc:`Requirements <requirements>`
* :doc:`Installation <requirements>`

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Getting Started

   requirements


Demo
----

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Demo

   demo

Configuration
-------------

* :doc:`Parameters <parameters>`

.. toctree::
   :maxdepth: 2
   :caption: Configuration 
   :hidden:

   parameters


Results
-------

* :doc:`Outputs <output>`

.. toctree::
   :maxdepth: 3
   :caption: Results
   :hidden:

   output

