Output
------


Mandatory Files
~~~~~~~~~~~~~~~

- **all_objects.RData** : R file containing all the objects created during the run
- **cor_parall_log.txt** : log file showing the parallelization progress during the correlation step
- **correlation_plots_xxxxxxxxxx.pdf** : correlation plots
- **cv1_cv2_rho1_rho2_backup.RData** : R backup file containing cv1, cv2, rho1 and rho2. This file that can be used by the cv.rho.obtained and path.cv.rho parameters 
        
    +--------+-------------------------------------------------------------------------------------+
    |**cv1** || selected coefficient of variation from observed matrix 1 (see plots_xxxxxxxxxx.pdf)|
    |        || used to generate the theoretical diagonals of theoretical matrix 1                 |
    +--------+-------------------------------------------------------------------------------------+
    |**cv2** | idem *cv1* but for matrix 2                                                         |
    +--------+-------------------------------------------------------------------------------------+
    |**rho1**|| Spearman coefficient of correlation between diagonal values                        |
    |        || from observed matrices 1 and 2 (see plots_xxxxxxxxxx.pdf)                          |
    |        || used to generate the theoretical diagonals of theoretical matrix 1 and 2           |
    +--------+-------------------------------------------------------------------------------------+
    |**rho2**| strictly identical to *rho1*                                                        |
    +--------+-------------------------------------------------------------------------------------+

- **inf_mask_pre_serp.txt** : matrix of the mask pre serpentine indicating contacts of the observed Mat2 cells significatively inferior to contacts of the observed Mat1 cells (Mat2 / Mat1 << 1)
- **mat1.theo.txt** : theoretical matrix 1 generated from the matrix 1 imported
- **mat2.theo.txt** : idem *mat1.theo.txt* but for matrix 2 
- **mask_pre_serp.txt** : matrix of the mask pre-serpentine indicating contacts of the observed Mat2 cells significatively superior or inferior to contacts of the observed Mat1 cells (Mat2 / Mat1 >> 1 or Mat2 / Mat1 << 1)
- **plots_xxxxxxxxxx.pdf** : all the plots, except the correlation plots 
- **segmentation_pre_serp.RData** : R file containing all the objects resulting from the analysis of significance pre serpentine 

    +---------------------------+---------------------------------------------------------------------------------------------------------------------------+
    |**obs**                    | | cell-cell comparison between observed matrix 1 and 2: MEAN, RATIO (mat2 / mat1), MATRICES (Obs or Theo, here Obs),      |
    |                           | | coord_1D (One dimension coordinate of the 2 compared cells in the obs matrices,                                         |
    |                           | | from 1 to nb rows x nb colums). Potentially log transformed depending on the `transfo` parameter setting                |
    +---------------------------+---------------------------------------------------------------------------------------------------------------------------+
    |**theo**                   | idem *obs* but for theoretical matrices                                                                                   |
    +---------------------------+---------------------------------------------------------------------------------------------------------------------------+
    |**signif.obs.dot.pre**     | significant cells after segmentation and after `ratio.limit.sig` parameter setting (extraction from obs)                  |
    +---------------------------+---------------------------------------------------------------------------------------------------------------------------+
    |**inf.signif.obs.dot.pre** | idem *signif.obs.dot.pre* but inly the cells for which matrix 2 cells << matrix 1 cells                                   |
    +---------------------------+---------------------------------------------------------------------------------------------------------------------------+
    |**sup.signif.obs.dot.pre** | idem *signif.obs.dot.pre* but inly the cells for which matrix 2 cells >> matrix 1 cells                                   |
    +---------------------------+---------------------------------------------------------------------------------------------------------------------------+
    |**signif.theo.dot.pre**    | idem *signif.obs.dot.pre* but for theoretical matrices                                                                    |
    +---------------------------+---------------------------------------------------------------------------------------------------------------------------+
    |**segment.pre.serp**       | | list containing all the results from the ``fun_segmentation()`` function coming from the cute little R function toolbox.|
    |                           | | See https://gitlab.pasteur.fr/gmillot/cute_little_R_functions for the details                                           |
    +---------------------------+---------------------------------------------------------------------------------------------------------------------------+

- **slitherine_xxxxxxxxxx_report.txt** : report file 
- **sup_mask_pre_serp.txt** : matrix of the mask pre serpentine indicating the observed Mat2 cells significatively superior observed Mat1 cells (Mat2 / Mat1 >> 1)


Optional Files 
~~~~~~~~~~~~~~

When ``serp_binning = TRUE``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- **inf_mask_post_serp.txt** : idem *inf_mask_pre_serp.txt* but post serpentine 
- **mask_post_serp.txt** : idem *mask_pre_serp.txt* but post serpentine 
- **mat1.obs.serp.txt** : first observed matrix (corresponding to file.name1 user parameter) after serpentine binning 
- **mat1.theo.serp.txt** : first theoretical matrix (associated to mat1.obs.serp.txt) after serpentine binning 
- **mat2.obs.serp.txt** : second observed matrix (corresponding to file.name2 user parameter) after serpentine binning 
- **mat2.theo.serp.txt** : second theoretical matrix (associated to mat2.obs.serp.txt) after serpentine binning 
- **obs_serp_parall_log.txt** : log file of the serpentine parallelization step 
- **segmentation_post_serp.RData** : idem *segmentation_pre_serp.RData* but post serpentine 
- **sup_mask_post_serp.txt** : idem *sup_mask_pre_serp.txt* but post serpentine 
- **theo_serp_parall_log.txt** : log file reporting all the information of the slitherine run 


When ``hiccomp = TRUE``
^^^^^^^^^^^^^^^^^^^^^^^

- **hicc_pvalue_mask_pre_serp.txt** : matrix of the mask pre serpentine obtained with the HiC Compare package and corresponding to the significant cells on the differential heatmap
- **hicc_padj_mask_pre_serp.txt** : idem *hicc_pvalue_mask_pre_serp.txt* but using adjusted p values (benjamini hochberg) 
- **hicc_pvalue_mask_post_serp.txt** : idem *hicc_pvalue_mask_pre_serp.txt* but after serpentine binning (serp.binning = TRUE) 
- **hicc_padj_mask_post_serp.txt** : idem *hicc_padj_mask_pre_serp.txt* but after serpentine binning (serp.binning = TRUE) 



Additional .RData files can be present, depending on the setting of parameter ``keep``. Such files are described in the *slitherine_xxxxxxxxxx_report.txt* file


