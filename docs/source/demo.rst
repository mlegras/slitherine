Demo
----


Configuration file
~~~~~~~~~~~~~~~~~~

.. code-block::

    in_path = "/path/to/data folder"
    out_path = "/path/to/output/folder"

The output folder is created by Slitherine. 

.. code-block::

    matrix_file1 = "name_matrix1.txt"
    matrix_file2 = "name_matrix2.txt"

If you want to bin matrices with Serpentine : 

.. code-block::

    serp_binning = true
    thr_tot = '70'
    thr_min = '7'



Command line
~~~~~~~~~~~~

.. prompt:: bash $

    nextflow slitherine.nf


