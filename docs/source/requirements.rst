Requirements
------------

- `Nextflow`_
- `Singularity`_

Nextflow requires Java 11 (or later) to be installed and to install Singularity you need to install some dependencies and Go.

.. _Nextflow: https://www.nextflow.io/docs/latest/getstarted.html
.. _Singularity: https://docs.sylabs.io/guides/3.0/user-guide/installation.html


Installation
------------










