Parameters
----------

**gimonkei wa zenbu kaenai to**

49 variables in nextflow.config 

+-----------------+-----------------+-------------------+-------------------+------------------+
|project_name     |thread_nb        |path_cv_rho        |activate_pdf       |transfo           |
+-----------------+-----------------+-------------------+-------------------+------------------+
|lib_path         |hiccomp          |correl_mat_obtained|optional_text      |warn_secu         |
+-----------------+-----------------+-------------------+-------------------+------------------+
|in_path          |binning          |path_theo1_theo2   |width_wind         |iter_nb           |
+-----------------+-----------------+-------------------+-------------------+------------------+
|out_path         |theo_import      |single_corr        |height_wind        |serp_symmet_input |
+-----------------+-----------------+-------------------+-------------------+------------------+
|matrix_file1     |theo_path_in     |print_count        |dot_size           |path_function1    |
+-----------------+-----------------+-------------------+-------------------+------------------+
|matrix_file2     |theo_file_name1  |keep               |line_size          |adj_mean          |
+-----------------+-----------------+-------------------+-------------------+------------------+
|serp_binning     |theo_file_name2  |ratio_limit_sig    |heatmap_text_size  |hiccompare_graph  |
+-----------------+-----------------+-------------------+-------------------+------------------+
|thr_tot          |n_row            |error              |text_size          |intern_funct_check|
+-----------------+-----------------+-------------------+-------------------+------------------+
|thr_min          |win_size         |step_factor        |title_text_size    |mask_plot         |
+-----------------+-----------------+-------------------+-------------------+------------------+
|empty_cell_string|cv_rho_obtained  |ratio_normalization|raster             |                  |
+-----------------+-----------------+-------------------+-------------------+------------------+

About the project
~~~~~~~~~~~~~~~~~

``project_name`` : **Slitherine**


Paths and files
~~~~~~~~~~~~~~~

- ``matrix_file1`` / ``matrix_file2`` : name of the first/second matrix file. Tables of integer, without a header and separated by tabulations. 

- ``in_path`` : absolute pathway of the folder containing the input data files (file.name1 and file.name2)

- ``out_path`` : absolute pathway of the destination folder that will contain the results (exported data)

- | ``lib_path`` : absolute pathway of the folder containing the R packages, NULL for the default path. 
  | **BEWARE**: default path is dependent on the system and interface used.  




Matrix structure
~~~~~~~~~~~~~~~~

- ``empty_cell_string`` : if the imported matrix are half filled, put here the character string ("-" for instance) or number (0 for instance) or reserved R word (NA for instance) used to fill the empty part of the matrix. Slitherine will automatically complete the imported matrix. Write NULL if the imported matrix are not half empty
- | ``thread_nb`` : Integer specifying the number of threads available. 
  | **BEWARE**: it is possible to have several threads per cpu (https://en.wikipedia.org/wiki/Thread_(computing)). 
  | Slitherine can parallelized some part of its job (including serpentine job) using thread.nb to speed up the run. If NULL, slitherine will take the number of threads available - 1. If non null, will use thread.nb if thread.nb <= number of threads available and number of threads available otherwise

Serpentine
~~~~~~~~~~

- ``serp_binning`` : TRUE or FALSE, do a binning with Serpentine

- ``thr_tot`` : serpentine threshold parameter

- ``thr_min`` : serpentine min threshold parameter

- ``iter_nb`` : total Serpentine iteration number parameter.         

- ``serp_symmet_input`` : true or false, true if matrices are symmetric.                     


HiCcompare
~~~~~~~~~~

- ``hiccomp`` : logical. HICcompare required? See https://bioconductor.org/packages/release/bioc/manuals/HiCcompare/man/HiCcompare.pdf. If TRUE, the binning parameter must be non null
- ``binning`` : integer that specifies the binning size of the imported matrices (in bp). If NULL, HiCcompare cannot be used


Theorical matrix computation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Theorical matrix 1 (THEO1) and theorical matrix 2 (THEO2) are synthetic matrices that are computed by Slitherine using data from the two oberved matrices. 
These theoretical matrices are used to define significant difference between the two observed matrices 
(i.e., pixel difference far from random fluctuations). 
Each THEO1 and THEO2 matrix are made of n colums, each column of THEO1 representing the diagonales of input matrix 1 (OBS1), 
and each column of THEO2 representing the diagonales of input matrix 2 (OBS2). 
Thus, the number of colums of column in THEO1 and THEO2 is defined by the dimension of the observed matrices. 
Now the number of rows of THEO1 and THEO2, set by the n.row parameter (see below), 
is a variable associated to power of the significance. 
Greater n.row is, greater is the puissance but longer is slitherine to achieve a run, 
in particularly during the permutation process. Of note, In observed matrices, if the dimension is 4, 
4 (square matrix) the first diagonal (main diagonal) is made of 4 values, and the last diagonal (corner diagonal) of 1 value. 
In theoretical matrices, such number of value is constant among the different columns representing 
the diagonals of the observed matrices

- ``theo_import`` : logical. Import theoretical matrices already obtained using slitherine? If TRUE, matrix comparison is preformed without serpentine binning (serp.binning automatically set to FALSE)
- ``theo_path_in`` : absolute pathway of the folder containing the input data files (file.name1 and file.name2)
- ``theo_file_name1`` : name of the first matrix file
- ``theo_file_name2`` : name of the second matrix file
- ``n_row`` : integer value setting the number of rows of the theoretical matrices. If NULL, n.row will take the number of column of the theoretical matrices
- ``win_size`` : nb of cv values taken in the sliding windows on the CV / MEAN plot to define an average cv at the rupture slope on the CV / MEAN plot (must be less than matrix dimension - 2). Increase this value if warning messages appears saying: "PARAMETER MUST BE SUCH THAT cv^2 > 1/mu"
- ``cv_rho_obtained`` : coefficient of variation (cv) of observed matrices 1 and 2, as well as correlation between observed matrices 1 and 2 already obtained ? If TRUE, will use the path.cv.rho parameter to load the data
- ``path_cv_rho`` : file and absolute pathway to download the cv1, cv2, as well as rho1 and rho2 (which are identical) of observed matrices 1 and 2 already obtained. Write NULL if not required. Not considered if cv.rho.obtained is FALSE
- ``correl_mat_obtained`` : theoretical matrix with permutation already obtained ? If TRUE, will use the path.theo1.theo2 parameter to load the matrices

- ``path_theo1_theo2`` : file and absolute pathway to download the theoretical matrix with permutation already obtained. Write NULL if not required. Not considered if correl.mat.obtained is FALSE

- ``single_corr`` : "MAX", "VALUE", "DEC1", "QUART1", "MED", "MIN" or "NO" c'est quoi??

+---------------------------+--------------------------------------------------------------------------------------------+
| single_corr value options | description                                                                                |
+===========================+============================================================================================+
|VALUE                      | | a unique arbitrary value, used as reference to generate the correlation between          |     
|                           | | the related diagonals of the theoretical theo1 and theo2 matrices                        |
|                           | | (all the correlations between theo1 and theo2 diagonals will be close to abs.corr.limit).| 
+---------------------------+--------------------------------------------------------------------------------------------+
| MAX                       | | the maximal correlation value between the observed mat1 and mat2 matrix diagonals        |
|                           | | will be used to generate the correlation between the related diagonals of theo1 and      |
|                           | | theo2 matrices (all the correlations between theo1 and theo2 diagonals                   |
|                           | | will be close to max(rho1)).                                                             |
+---------------------------+--------------------------------------------------------------------------------------------+
| DEC1                      | | same as MAX but using the first decile correlation value between the observed            |
|                           | | mat1 and mat2 matrix diagonals, respectively.                                            |
+---------------------------+--------------------------------------------------------------------------------------------+
| QUART1                    | | same as MAX but using the first quartile correlation value between the observed          |
|                           | | mat1 and mat2 matrix diagonals, respectively.                                            |
+---------------------------+--------------------------------------------------------------------------------------------+
| MED                       | | same as MAX but using the median correlation value between the observed                  |
|                           | | mat1 and mat2 matrix diagonals, respectively.                                            |
+---------------------------+--------------------------------------------------------------------------------------------+
| MIN                       | | same as MAX but using the minimal correlation value between the observed                 |
|                           | | mat1 and mat2 matrix diagonals, respectively.                                            |
+---------------------------+--------------------------------------------------------------------------------------------+
| NO                        | | each of the observed correlations between the related diagonals of                       |
|                           | | the mat1 and mat2 matrices will be used to generate                                      |
|                           | | the correlation of the corresponding theo1 and theo2 diagonal.                           |
+---------------------------+--------------------------------------------------------------------------------------------+
    
    In the case of NO, any observed correlation below the abs.corr.limit parameter will be set to abs.corr.limit 
    (to avoid very long computing needed for very weak correlations)

- ``print_count`` : during the correlation adjustment process, print a message every print.count loops ?
- ``keep`` : keep the intermediate matrices and big objects in the working environment til the end? If TRUE, everything is saved in the final all_objects.RData. If FALSE, intermediate matrices are saved in different .RData files and then removed all along the script execution


Significative regions between the 2 compared matrices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- ``ratio_limit_sig`` : ratio value between the two matrice pixel, below which ratio is not significant? From 1 to +Inf (2 means coverage ratio less than 2 is not significant, 1 means no cutoffs in ratio values)
- ``error`` : from 0 to 1. Proportion of false positives (i.e., theo dots considered as observed dots). 0.05 means 5%, 0 means that the significant observed dot are outside of the theo cloud

- ``step_factor`` : for the significant dots. x.win.size / step.factor gives the shift step of the window. When step.factor = 1, no overlap during the sliding. If step.factor = 2, 50% of overlap during 1 slide

(range.split : for the significant dots. 
If x.range is the range of the dots on the x-axis, then abs(diff(x.range) / range.split) gives the window size. 
Window size decreases when range.split increases)

- ``ratio_normalization`` : logical. Divide the cell ratio matrix mat2 / mat1 (differential matrix) by the ratio factor mean(mat2) / mean(mat1)? If TRUE, this means that the mean of the normalized cell ratio matrix is 1, and log (parameter transfo <- TRUE)is 0



Graphical and display parameters             
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


- ``activate_pdf`` : write TRUE for pdf display and FALSE for R display (main graphs)
- ``optional_text`` : write here an optional text to include in results and graphs
- ``width_wind`` : window width (in inches)
- ``height_wind`` : window height (in inches)
- ``dot_size`` : increase or decrease the value to increase or decrease the size of the dots
- ``line_size`` : increase or decrease the value to increase or decrease the size of the lines
- ``heatmap_text_size`` : increase or decrease the value to increase or decrease the size of the heatmap scale text
- ``text_size`` : increase or decrease the value to increase or decrease the size of the axis text and legend text
- ``title_text_size`` : increase or decrease the value to increase or decrease the size of the title text
- ``raster`` : raster mode for dot plots ?
- | ``transfo`` : "log2", "log10", "no"
  | Log is only applied for display, the reason why the option "no" is not proposed (which would mean )

+----------------------------------------+-----------------------------------------+----------------------------+
| log2                                   |  log10                                  | no                         |
+========================================+=========================================+============================+
|| matrix values will be log2 converted, | | matrix values will be log10 converted | data already log converted |
|| and sometimes log2(x +1) converted,   | | and sometimes log10(x +1) converted,  |                            |
|| +1 to deal with zero                  | | +1 to deal with zero                  |                            |
+----------------------------------------+-----------------------------------------+----------------------------+

  mettre un tableau 


- ``warn_secu`` : true or false Display if internal functions of anova_contrasts have variables that are present in other environments.      


Functions
~~~~~~~~~

- ``path_function1`` : file (and absolute pathway) of the required cute_little_R_functions toolbox.
- ``adj_mean``
- ``hiccompare_graph``
- ``intern_funct_check``
- ``mask_plot``
