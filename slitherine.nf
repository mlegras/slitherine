#! /usr/bin/env nextflow
/*
#########################################################################################
##                                                                                     ##
##     script.nf                                                                       ##
##     Slitherine                                                                    ##
##                                                                                     ##
##     Mia Legras                                                                      ##
##     Bioinformatics and Biostatistics Hub                                            ##
##     Computational Biology Department                                                ##
##     Institut Pasteur Paris                                                          ##
##                                                                                     ##
#########################################################################################
*/


//      Variables

config_file = file("${projectDir}/nextflow.config")
log_file = file("${launchDir}/.nextflow.log")

//      Variables from config.file that need to be checked
matrix1_ch_test = file("${in_path}/${matrix_file1}") // to test if exist below
matrix2_ch_test = file("${in_path}/${matrix_file2}")

//      end Variables


//      Channels

matrix_ch1 = Channel.fromPath("${in_path}/${matrix_file1}", checkIfExists: false) //to perform the check below, in order to have a more explicit error message
matrix_ch2 = Channel.fromPath("${in_path}/${matrix_file2}", checkIfExists: false)
if(theo_import){ //if theo_import == true
    theo_mat_ch1 = Channel.fromPath("${in_path}/${theo_file_name1}")
    theo_mat_ch2 = Channel.fromPath("${in_path}/${theo_file_name2}")
}
//      end Channels

//      Checks

if(system_exec == 'local' || system_exec == 'slurm'){
    def file_exists1 = matrix1_ch_test.exists()
    def file_exists2 = matrix2_ch_test.exists()
    if( ! file_exists1 && ! file_exists2){
        print("\n\nERROR IN NEXTFLOW EXECUTION\n\n"
            + "INVALID in_path AND matrix_file PARAMETERS IN nextflow.config FILE: "
            + "${matrix_file1} or ${matrix_file2}\n\n"
            + "IF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n")
    }
} else{
    print("\n\nERROR IN NEXTFLOW EXECUTION\n\n"
            + "INVALID system_exec PARAMETER IN nextflow.config FILE: ${system_exec}\n"
            + "THE ONLY POSSIBLE VALUES ARE local, slurm OR slurm_local\n\n")
}

//      end Checks



//      Process
process PRE_SERPENTINE {
    label 'R'
    publishDir "${out_path}", mode: 'copy', pattern: "{*.txt, *.RData, *.pdf}", overwrite: false 
    // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    cache 'true'

    input:
    stdin in_path
    stdin out_path
    val project_name
    val lib_path
    stdin path_function1
    file mat1 from matrix_ch1 //file.name1
    file mat2 from matrix_ch2 //file.name2
    val empty_cell_string
    val thread_nb 
    val hiccomp 
    val binning 
    val theo_import 
    val n_row 
    val win_size 
    val cv_rho_obtained 
    stdin path_cv_rho 
    val correl_mat_obtained 
    stdin path_theo1_theo2 
    val single_corr 
    val print_count 
    val keep 
    val ratio_limit_sig 
    val error 
    val range_split 
    val step_factor 
    val ratio_normalization 
    val activate_pdf 
    val optional_text 
    val width_wind 
    val height_wind 
    val dot_size 
    val line_size 
    val heatmap_text_size 
    val text_size 
    val title_text_size 
    val raster 
    val transfo 
    val warn_secu
    val serp_symmet_input
    val adj_mean
    val hiccompare_graph
    val intern_funct_check
    val mask_plot
    if(theo_import){
        file mat1_theo from theo_mat_ch1
        file mat2_theo from theo_mat_ch2
        path theo_path_in
    }
    
    output:
    path "mat1.obs.txt" into matrix_norm_ch1
    path 'mat2.obs.txt' into matrix_norm_ch2
    path 'mat1.theo.txt' into matrix_theo_ch1
    path 'mat2.theo.txt' into matrix_theo_ch2
    path '*.RData'
    path '*.pdf'

    script:
    if(theo_import==true)
        """
        slith_part1.R "${in_path}" "${out_path}" "${project_name}" "${lib_path}" "${path_function1}" \
            "${mat1}" "${mat2}" "${empty_cell_string}" "${thread_nb}" "${hiccomp}" \
            "${binning}" "${theo_import}" "${n_row}" "${win_size}" "${cv_rho_obtained}" \
            "${path_cv_rho}" "${correl_mat_obtained}" "${path_theo1_theo2}" "${single_corr}" \
            "${print_count}" "${keep}" "${ratio_limit_sig}" "${error}" "${range_split}" \
            "${step_factor}" "${ratio_normalization}" "${activate_pdf}" "${optional_text}" \
            "${width_wind}" "${height_wind}" "${dot_size}" "${line_size}" "${heatmap_text_size}" \
            "${text_size}" "${title_text_size}" "${raster}" "${transfo}" "${warn_secu}" \
            "${serp_symmet_input}" "${adj_mean}" "${hiccompare_graph}" "${intern_funct_check}" "${mask_plot}" \ 
            "${mat1_theo}" "${mat2_theo}" "${theo_path_in}"
        """
    else
        """
        slith_part1.R "${in_path}" "${out_path}" "${project_name}" "${lib_path}" "${path_function1}" \
            "${mat1}" "${mat2}" "${empty_cell_string}" "${thread_nb}" "${hiccomp}" \
            "${binning}" "${theo_import}" "${n_row}" "${win_size}" "${cv_rho_obtained}" \
            "${path_cv_rho}" "${correl_mat_obtained}" "${path_theo1_theo2}" "${single_corr}" \
            "${print_count}" "${keep}" "${ratio_limit_sig}" "${error}" "${range_split}" \
            "${step_factor}" "${ratio_normalization}" "${activate_pdf}" "${optional_text}" \
            "${width_wind}" "${height_wind}" "${dot_size}" "${line_size}" "${heatmap_text_size}" \
            "${text_size}" "${title_text_size}" "${raster}" "${transfo}" "${warn_secu}" "${serp_symmet_input}" \
            "${adj_mean}" "${hiccompare_graph}" "${intern_funct_check}" "${mask_plot}"
        """
} 

if (serp_binning == true){
    process SERPENTINE {
        label 'python'
        publishDir "${out_path}", mode: 'copy', pattern: "{*.csv}", overwrite: false 
        // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
        cache 'true'

        input:
        path matrix1 from matrix_norm_ch1
        path matrix2 from matrix_norm_ch2
        path matrix1_theo from matrix_theo_ch1
        path matrix2_theo from matrix_theo_ch2
        val thr_tot
        val thr_min

        output:
        path 'mat1_bin.csv' into matrixbin_ch1
        path 'mat2_bin.csv' into matrixbin_ch2
        path 'mat1_theo_bin.csv' into matrixbin_theo_ch1
        path 'mat2_theo_bin.csv' into matrixbin_theo_ch2    

        script:
        """
        use_serp.py "${matrix1}" "${matrix2}" "${matrix1_theo}" "${matrix2_theo}" "${thr_tot}" "${thr_min}"
        """
    }

    process POST_SERPENTINE {
        label 'R'
        publishDir "${out_path}", mode: 'copy', pattern: "{*.txt, *.RData, *.pdf}", overwrite: false 
        // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
        cache 'true'

        input:
        stdin in_path
        stdin out_path
        val project_name
        stdin path_function1
        path mat1obs from matrixbin_ch1 
        path mat2obs from matrixbin_ch2 
        path mat1theo from matrixbin_theo_ch1 
        path mat2theo from matrixbin_theo_ch2 
        val hiccomp 
        val binning 
        val keep 
        val ratio_limit_sig 
        val error 
        val range_split 
        val step_factor 
        val ratio_normalization 
        val activate_pdf 
        val optional_text 
        val width_wind 
        val height_wind 
        val dot_size 
        val line_size 
        val heatmap_text_size 
        val text_size 
        val title_text_size 
        val raster 
        val transfo 
        val serp_symmet_input
        val adj_mean
        val hiccompare_graph
        val intern_funct_check
        val mask_plot

        output:
        path "*.txt"
        path "*.RData"
        path "*.pdf"

        script:
            """
            slith_part2.R "${project_name}" "${in_path}" "${out_path}" "${path_function1}" \
                "${mat1obs}" "${mat2obs}" "${mat1theo}" "${mat2theo}" "${hiccomp}" \
                "${binning}" "${keep}" "${ratio_limit_sig}" "${error}" "${range_split}" \
                "${step_factor}" "${ratio_normalization}" "${activate_pdf}" "${optional_text}" \
                "${width_wind}" "${height_wind}" "${dot_size}" "${line_size}" "${heatmap_text_size}" \
                "${text_size}" "${title_text_size}" "${raster}" "${transfo}" "${warn_secu}" \
                "${serp_symmet_input}" "${adj_mean}" "${hiccompare_graph}" "${intern_funct_check}" "${mask_plot}"
            """
    }
}

//      end Processes
